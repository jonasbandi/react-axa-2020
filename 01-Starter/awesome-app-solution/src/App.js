import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Greeter} from './Greeter';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Greeter />
      </header>
    </div>
  );
}

export default App;
