import React, { Suspense, lazy } from "react";
import { BrowserRouter as Router, Link, Route, Routes } from "react-router-dom";
import "./App.scss";
// import { Home } from "./home/Home";
// import { About } from "./about/About";

// Note: code-splitting with dynamic import() is optional. The components could also be imported statically.
const Home = lazy(() => import("./home/Home"));
const About = lazy(() => import("./about/About"));

function App() {
  return (
    <div>
      <Suspense fallback={<h3>Loading ...</h3>}>
        <Router>
          <div className="App">
            <header className="App-header">
              <ul>
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li>
                  <Link to="about">About</Link>
                </li>
              </ul>
            </header>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="about" element={<About />} />
              <Route path="about/:name" element={<About />} />
            </Routes>
          </div>
        </Router>
      </Suspense>
    </div>
  );
}

export default App;
