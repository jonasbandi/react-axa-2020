import React from "react";
import { useNavigate, useLocation, useParams } from "react-router-dom";

import styles from "./About.module.scss";

export default function About() {
  const params = useParams();
  const location = useLocation();
  const navigate = useNavigate();

  return (
    <div className={styles.comic}>
      <h1>About</h1>
      <p>This is the About component.</p>
      <br />
      <div>Router info:</div>
      <pre>{JSON.stringify(params)}</pre>
      <pre>{JSON.stringify(location)}</pre>
      <button onClick={() => navigate("/")}>Go Home</button>
    </div>
  );
}
