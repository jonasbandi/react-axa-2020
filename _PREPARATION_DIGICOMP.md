# React Course: Software Installation
For the course the following software should be installed.  

**The version numbers don't have to match exactly, but they should be pretty close!**



### Git

A recent git installation is needed to download the sample code and exercises.  
Git can be downloaded from here: <https://git-scm.com/download/>

Check:  

	> git --version                                                             
	git version 2.25.0



### Node.js & NPM 
Node and NPM are the fundament for the JavaScript toolchain.  
The Node installer can be downloaded here: <https://nodejs.org/>

Install the latest version of Node.  

Check:

	> node --version
	v13.10.0
	> npm --version
	6.14.2



### Global NPM Packages

We want to install some JavaScript development tools globally, so that they can be used from the commandline.

**Windows:** run the following commands in a Administrator command prompt:

	npm install -g create-react-app serve 


​	Check:

```
create-react-app --version
> 3.4.0
serve --version
> 11.3.0
```



### Browser

Install the latest Chrome Browser from: https://www.google.com/chrome/  



### WebStorm and Visual Studio Code
Install WebStorm Trial Versiom from http://www.jetbrains.com/webstorm.

Install Visual Studio Code from: https://code.visualstudio.com/

Make sure that both applications can be started with the login of the course participants.



### Course Material

Create a directory `D:\Data\` and clone the repo like this:

	git clone https://bitbucket.org/jonasbandi/react-axa-2020.git


Make sure that course participants can pull from the repo:

	cd react-axa-2020
	git pull



### Run the intro example

In order to check that the environment is set up correctly, the intro example should be executed. In `D:\Data\react-axa-2020` run the following commands:

	cd 01-Starter/awesome-app
	npm install
	npm start

With the last command a browser window should open on `localhost:3000` showing the React logo.

