class Counter extends React.Component {
  state = {
    count: 0
  };

  componentDidMount() {
    console.log('Counter did mount.');
    this.setState({ count: this.state.count + 2 });
  }

  componentDidUpdate() {
    console.log('Counter did update.');
  }

  componentWillUnmount() {
    console.log('Counter will unmount...');
  }

  render() {
    console.log('Counter rendering ...');
    return (
      <div>
        <h3>Lifecycle Example</h3>
        <p>
          Count:
          <b> {this.state.count} </b>
        </p>
      </div>
    );
  }
}

class App extends React.Component {
  state = { showCounter: true };

  toggle() {
    this.setState({ showCounter: !this.state.showCounter });
  }

  render() {
    console.log('App rendering ...');
    return (
      <div>
        <button onClick={() => this.toggle()}>Toggle!</button>
        {this.state.showCounter ? <Counter /> : null}
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));

// DEMO:
// - show componentWillMount
// - schedule the counter increment with setInterval, show leak
// - implement componentWillUnmount using this.timer
//
// - update the App periodically (i.e. introduce new state 'date').
// - show how rendering cascades and counter componentDidUpdate is called
// - implement shouldComponentUpdate(nextProps, nextState)
