
const {useRef} = React;
function AppComponent(props) {

  const inputIsGoingRef = useRef(null);
  const inputNumberOfGuestsRef = useRef(null);


  function submitForm(e) {
    e.preventDefault();

    // using DOM-API to get the value
    console.log(e.target.elements.isGoing.checked);
    console.log(e.target.elements.numberOfGuests.value);

    // using refs to get the value
    console.log(inputIsGoingRef.current.checked);
    console.log(inputNumberOfGuestsRef.current.value);
  }


  return (
    <form onSubmit={e => submitForm(e)}>
      <label>
        Is going:
        <input
          name="isGoing"
          ref={inputIsGoingRef}
          type="checkbox"
          defaultChecked={props.initialIsGoing}
        />
      </label>
      <br/>
      <label>
        Number of guests:
        <input
          name="numberOfGuests"
          ref={inputNumberOfGuestsRef}
          type="number"
          defaultValue={props.initialNumberOfGuests}
        />
      </label>
      <br/>
      <button type="submit">Submit</button>
    </form>
  );
}

const app = <AppComponent initialNumberOfGuests={42} initialIsGoing={true}/>;

ReactDOM.render(app, document.getElementById("root"));
