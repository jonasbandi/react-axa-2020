const {useState, useEffect} = React;


const COUNTRIES = ['France', 'Germany', 'Italy', 'India', 'Indonesia', 'Switzerland', 'Sweden'];

function Spinner() {
  return (
    <div className="dot-spinner">
      <div className="bounce1"/>
      <div className="bounce2"/>
      <div className="bounce3"/>
    </div>
  );
}

function withLoadingSpinner(Component) {
  return function (props) {
    if (props.isLoading) {
      return <Spinner/>;
    } else {
      return <Component {...props}/>;
    }
  };
}

function Title(props) {
  return <h1>{props.title}</h1>;
}

function Clock({time, hourOffset}) {
  const displayTime = moment(time).add(hourOffset, 'h').toString();
  return (
    <h3>
      {displayTime}
    </h3>
  );
}


const TitleWithLoadingSpinner = withLoadingSpinner(Title);
const ClockWithLoadingSpinner = withLoadingSpinner(Clock);

function AppComponent() {

  const [title, setTitle] = useState('');
  const [countries, setCountries] = useState(COUNTRIES.map(c => ({name: c, time: undefined})));

  function scheduleNextTimestamp() {
      const countryWithoutTimestamp = countries.find(c => !c.time);
      if (countryWithoutTimestamp){
        setTimeout(() => {
          setCountries(countries.map(c => c !== countryWithoutTimestamp ? c : {...c, time: new Date()}));
        }, 2000);
      }
  }


  useEffect(
    () => {
      setTimeout(() => setTitle('World Clock'), 1000)
    }, []);

  useEffect(
    () => {
      scheduleNextTimestamp()
    }, [countries]);

  return (
    <div>
      <TitleWithLoadingSpinner title={title} isLoading={!title}/>
      <br/>
      {
        countries
          .map((country, index) => (
            <div key={index}>
              Time in {country.name}: <ClockWithLoadingSpinner time={country.time} hourOffset={index}
                                                               isLoading={!country.time}/>
            </div>
          ))
      }
    </div>
  );
}


const app = <AppComponent/>;

ReactDOM.render(
  app,
  document.getElementById('root')
);

