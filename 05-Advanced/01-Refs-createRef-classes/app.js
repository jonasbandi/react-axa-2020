class GreeterComponent extends React.Component {

  buttonRef1 = React.createRef();
  buttonRef2 = React.createRef();
  buttonRef3 = React.createRef();

  render() {
    return (
      <div>
        <h1>React works with a virtual DOM</h1>
        <button ref={this.buttonRef1}>
          Button 1
        </button>
        <br />
        <button ref={this.buttonRef2}>
          Button 2
        </button>
        <br />
        <button ref={this.buttonRef3}>
          Button 3
        </button>
      </div>
    );
  }

  componentDidMount() {
    this.buttonRef2.current.focus();
  }
}

ReactDOM.render(<GreeterComponent />, document.getElementById("root"));

// DEMO:
// - create ref to title, apply jquery-fittext plugin:
// $(this.titleRef.current).fitText(1.5);
