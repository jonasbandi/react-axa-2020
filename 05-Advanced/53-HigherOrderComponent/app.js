function withBackground(Component){
  return (props) => (
    <div className="container">
      <hr/>
        <Component/>
      <hr/>
    </div>
  )
}

function Content() {
  return (
    <div>Test!</div>
  )
}

const WrapperComponent = withBackground(Content);

function App() {
  return (
    <WrapperComponent/>
  );
}

ReactDOM.render(<App/>, document.getElementById("root"));

